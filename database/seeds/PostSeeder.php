<?php

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
           [
                'uri' => 'instalando-laravel-centos7',
                'titulo' => 'Instalando Laravel no CentOS',
                'conteudo' => 'Teste Teste Teste Teste',
                'img_mini' => 'post-13.jpg',
                'perfil_id' => 2,
                'caregoria_id' => 3,
                'tags' => 'CENTOS7, LARAVEL' 
            ],



        ]);
    }
}
