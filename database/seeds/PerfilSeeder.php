<?php

use Illuminate\Database\Seeder;

class PerfilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('perfis')->insert([
           [
            'nome' => 'Felipe Brasil',
			'uri' => 'felipe-brasil',
			'descricao' => 'TESTES TESTE'
            ],
            [
            'nome' => 'Filipe Crespo',
			'uri' => 'filpe-crespo',
			'descricao' => 'TESTES TESTE'
            ],
            [
            'nome' => 'João David',
			'uri' => 'joao-david',
			'descricao' => 'TESTES TESTE'
            ],
        ]);
    }
}
