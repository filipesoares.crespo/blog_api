<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;

class PostController extends Controller
{
    
    public function show($uri)
    {

    	$post = Post::where('uri',$uri)->first();

    	if ( empty($post) ){
        	
        	return response()->json(['error' => 'Error msg'], 404);

		}
		return response()->json([
		   'post' => $post
		]);

        
    }

}
